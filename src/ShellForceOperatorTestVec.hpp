#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
    /**
     * \addtogroup operators
     * @{
     **/

    namespace tag
    {
        template <class GV>
        struct shellForceVec {
            GV gridView;
            std::vector<int> partitions;
        };
    }


    /// zero-order vector-operator \f$ (\mathbf{b}\cdot\Psi) \f$
    template<class GV>
    class ShellForceRHSVec
    {
        std::vector<int> partitions_;
        GV gridView_; //the level 0 gridView of the grid!
    public:
        ShellForceRHSVec(tag::shellForceVec<GV> t)
          : partitions_(t.partitions), gridView_(t.gridView)
          {}

        template <class CG, class Node, class Quad, class LocalFct, class Vec>
        void assemble(CG const& contextGeo, Node const& node, Quad const& quad,
                      LocalFct const& localFct, Vec& elementVector) const {
            static_assert(static_size_v<typename LocalFct::Range> == CG::dow,
                          "Expression must be of vector type.");
            static_assert(Node::isPower,
                          "Operator can be applied to Power-Nodes only.");

            assert(node.degree() == CG::dow);

            std::size_t size = node.child(0).size();

            auto intersection = contextGeo.context();
            if (!intersection.neighbor())
                return;

            auto const &indexSet = gridView_.indexSet();
            auto fatherInside = intersection.inside(); //context MUST be Intersection here
            auto fatherOutside = intersection.outside();
            while (fatherInside.hasFather())
                fatherInside = fatherInside.father();
            while (fatherOutside.hasFather())
                fatherOutside = fatherOutside.father();
            int p = partitions_[indexSet.index(fatherInside)];
            int q = partitions_[indexSet.index(fatherOutside)];

            // apply the operator only to intersections on the shell
            if (p == 1 && q == 0) {
                auto geo = intersection.geometry(); //inside().template subEntity<1>(intersection.indexInInside()).geometry();
                for (auto const &qp : quad) {
                    // Position of the current quadrature point in the reference element
                    auto&& local = contextGeo.coordinateInElement(qp.position());
                    //const auto ds = (geo.corner(1) - geo.corner(0)).two_norm() * qp.weight();
                    // The multiplicative factor in the integral transformation formula
                    const auto ds = geo.integrationElement(qp.position()) * qp.weight();
                    const auto surfTen = localFct(local);

                    auto const& shapeValues = node.child(0).localBasisValuesAt(local);

                    for (std::size_t i = 0; i < size; ++i) {
                        for (std::size_t k = 0; k < CG::dow; ++k) {
                            const auto local_ki = node.child(k).localIndex(i);
                            elementVector[local_ki] += surfTen[k] * shapeValues[i] * ds;
                        }
                    }
                }
            }
        }
    };

    template <class LC, class GV>
    struct GridFunctionOperatorRegistry<tag::shellForceVec<GV>, LC>
    {
        static constexpr int degree = 0;
        using type = ShellForceRHSVec<GV>;
    };
    /** @} **/

} // end namespace AMDiS