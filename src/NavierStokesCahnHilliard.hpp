/**
 * Navier Stokes Cahn Hilliard Problem to be solved in every time step.
 *
 * The result is a 2D/3D velocity (_v) and a 1D pressure (_p). The basis for the velocity is of type lagrange<2>()
 * and the basis for the pressure is divided into subdomains, one for the internal and one for the external fluid
 * with respect to the shell (if the shell is closed). If the shell is not closed, the basis is defined using the
 * partitions vector, which then has to be modified s.t. it is equal to -1 apart from the surface.
 * The resulting velocity is continuous where the pressure is discontinuous over the shell.
 *
 * In addition to the Navier Stokes, a Cahn-Hilliard equation is solved in the system, resulting in a phase field,
 * which is between zero and 1. The Cahn-Hilliard equations are solved on the same basis as the pressure
 *
 * created by Marcel Mokbel 2020
**/

#pragma once

#include <memory>
#include <vector>

#include <amdis/extensions/BaseProblemInterface.hpp>

namespace AMDiS
{
    template <class HostGrid, class PBF>
    class NavierStokesCahnHilliard
            : public BaseProblemInterface
            , public ProblemInstatBase
    {
    public:
        using WorldVector = FieldVector<double, HostGrid::dimensionworld>;
        using AdaptiveGrid = AdaptiveGrid_t<HostGrid>;
        using GridView = typename AdaptiveGrid::LeafGridView;

        using Basis = decltype(GlobalBasis{std::declval<GridView>(),std::declval<PBF>()});
        using Problem = ProblemStat<DefaultProblemTraits<Basis>>;// TYPEOF(ProblemStat{"prob", std::declval<HostGrid&>(), std::declval<GlobalBasis>()});
        using Super = BaseProblemInterface;

        using PBFDOW = decltype(power<2>(lagrange<2>()));
        using VBasis = decltype(GlobalBasis{std::declval<GridView>(),std::declval<PBFDOW>()});
        using PBFC = decltype(lagrange<2>());
        using PhiBasisCont = decltype(GlobalBasis{std::declval<GridView>(),std::declval<PBFC>()});
        using PBF1 = decltype(surface(lagrange<2>(),std::declval<std::vector<int>>()));
        using PhiBasis = decltype(GlobalBasis{std::declval<GridView>(),std::declval<PBF1>()});

    public:
        enum { dow = HostGrid::dimensionworld };
        enum { dim = HostGrid::dimension };

        static constexpr auto _v = Dune::Indices::_0;
        static constexpr auto _p = Dune::Indices::_1;
        static constexpr auto _phi = Dune::Indices::_2;
        static constexpr auto _mu = Dune::Indices::_3;
        static constexpr auto _xS = Dune::Indices::_4;
        static constexpr auto _kappaVec = Dune::Indices::_5;
        static constexpr auto _lambda1 = Dune::Indices::_6; // artificial surfTen in incompressible case
        static constexpr auto _lambda2 = Dune::Indices::_7;
        static constexpr auto _fShell = Dune::Indices::_8;
        static constexpr auto _vS = Dune::Indices::_9;
        static constexpr auto _phiS = Dune::Indices::_10;

    public:
        NavierStokesCahnHilliard(std::string const& name,
                                 HostGrid& grid,
                                 PBF& pbf,
                                 std::vector<int> boundaryIDs,
                                 std::vector<int> partitions);

        ///Overriding Methods
        /// @brief Return the name of the BaseProblem. Implements \ref ProblemIterationInterface::name()
        std::string const& name() const override { return name_; }

        /// @biref Initialisation of the problem.
        void initialize(Flag initFlag,
                        BaseProblemInterface* adoptProblem = nullptr,
                        Flag adoptFlag = INIT_NOTHING) override
        {
            if (adoptProblem != nullptr) {
                auto* baseProblem = dynamic_cast<NavierStokesCahnHilliard*>(adoptProblem);
                test_exit(baseProblem != nullptr, "Can not determine BaseProblem type in initialization.");
                problem_.initialize(initFlag, &baseProblem->problem(), adoptFlag);
            } else {
                problem_.initialize(initFlag);
            }
        }

        /// @biref Method is called at the end of \ref initBaseProblem
        void finalizeData(AdaptInfo& /*adaptInfo*/) override { /* do nothing */ }

        /// @brief Implementation of \ref ProblemTimeInterface::transferInitialSolution()
        void transferInitialSolution(AdaptInfo& adaptInfo) override
        {
            writeFiles(adaptInfo);
        }


        /// @brief Implementation of \ref ProblemTimeInterface::closeTimestep()
        void closeTimestep(AdaptInfo& adaptInfo) override;

        /// @brief prepare iteration
        void beginIteration(AdaptInfo& /*adaptInfo*/) override
        {
            msg("");
            msg("[[ <{}> iteration ]]", name_);
        }

        /// @brief compute iteration
        Flag oneIteration(AdaptInfo& adaptInfo, Flag toDo = FULL_ITERATION) override
        {
            Flag flag;

            problem_.beginIteration(adaptInfo);
            flag |= problem_.oneIteration(adaptInfo, toDo);
            problem_.endIteration(adaptInfo);

            return flag;
        }

        /// @brief end iteration
        void endIteration(AdaptInfo& /*adaptInfo*/) override
        {
            msg("");
            msg("[[ end of <{}> iteration ]]", name_);
        }

        /// @brief Calls writeFiles of the problem
        void writeFiles(AdaptInfo& adaptInfo) override
        {
            problem_.writeFiles(adaptInfo, false);
        }

        /// @brief intiialize the data for the current time step
        void initTimestep(AdaptInfo& adaptInfo) override;

        /// @brief intitalize the data for the simulation
        void initData(AdaptInfo& adaptInfo) override;

        /// @brief intitalize the data for the simulation
        template <class Lambda>
        void initData(AdaptInfo& adaptInfo, Lambda const& lambda, double& vMin);

        /// @brief adds the operators of the equations to the system
        void fillOperators(AdaptInfo& adaptInfo) override;

        /// @bried adds the boundary conditions to the system
        void fillBoundaryConditions(AdaptInfo& adaptInfo) override;

        /// @brief builds the initial phase field and performs initial refinement
        void solveInitialProblem(AdaptInfo& adaptInfo) override;

        void assembleAndSolve(AdaptInfo& adaptInfo) {
            Flag toDo = FULL_ITERATION;
            Flag flag = 0, markFlag = 0;

            /// assemble and solve
            if (toDo.isSet(BUILD))
                problem().buildAfterAdapt(adaptInfo, markFlag, true, true);

            if (toDo.isSet(BUILD_RHS))
                problem().buildAfterAdapt(adaptInfo, markFlag, false, true);

            // solve nschProb!
            if (toDo.isSet(SOLVE))
                problem().solve(adaptInfo, true, false);

            if (toDo.isSet(SOLVE_RHS))
                problem().solve(adaptInfo, true, false);

            if (toDo.isSet(ESTIMATE))
                problem().estimate(adaptInfo);
        }

        void buildAndAdapt(AdaptInfo& adaptInfo) {
            Flag toDo = FULL_ITERATION;
            Flag flag = 0, markFlag = 0;

            ///buildAndAdapt
            if (toDo.isSet(MARK))
                markFlag = problem().markElements(adaptInfo);

            if (toDo.isSet(ADAPT) && markFlag.isSet(MESH_ADAPTED))
                flag |= problem().adaptGrid(adaptInfo);
        }

    public: /// getting methods
        /// velocity vector components
        auto getVelocity(std::size_t i)       { return this->solution(_v,i); }
        auto getVelocity(std::size_t i) const { return this->solution(_v,i); }

        /// velocity vector
        auto getVelocity()       { return this->solution(_v); }
        auto getVelocity() const { return this->solution(_v); }

        /// Velocity vector components at old timestep
        auto getOldVelocity(std::size_t i)        { return valueOf(*oldVelocity_,i); }
        auto getOldVelocity(std::size_t i) const  { return valueOf(*oldVelocity_,i); }

        /// velocity vector at old timestep
        auto getOldVelocity()        { return valueOf(*oldVelocity_); }
        auto getOldVelocity() const  { return valueOf(*oldVelocity_); }

        /// phase field component
        auto getPhase(int = 0)       { return this->solution(_phi);  }
        auto getPhase(int = 0) const { return this->solution(_phi);  }

        /// phase field component at old time step
        auto getOldPhase(int = 0)       { return valueOf(*oldPhi_); }
        auto getOldPhase(int = 0) const { return valueOf(*oldPhi_); }

        /// pressure component
        auto getPressure()       { return this->solution(_p); }
        auto getPressure() const { return this->solution(_p); }

        /// mu component
        auto getW(int = 0)       { return valueOf(*oldMu_); }
        auto getW(int = 0) const { return valueOf(*oldMu_); }

        /// Return a pointer to the grid
        std::shared_ptr<AdaptiveGrid>       grid()       { return problem_.grid(); }
        std::shared_ptr<AdaptiveGrid const> grid() const { return problem_.grid(); }

        /// Return the gridView of the leaf-level
        GridView gridView() const { return problem_.gridView(); }

        /// Return the boundary manager to identify boundary segments
        std::shared_ptr<BoundaryManager<AdaptiveGrid>>       boundaryManager()       { return problem_.boundaryManager(); }
        std::shared_ptr<BoundaryManager<AdaptiveGrid> const> boundaryManager() const { return problem_.boundaryManager(); }

        /// Return the GlobalBasis of the Problem
        std::shared_ptr<Basis>       globalBasis()       { return problem_.globalBasis(); }
        std::shared_ptr<Basis const> globalBasis() const { return problem_.globalBasis(); }

        /// Return the number of problems this BasieProblem contains. Implement \ref ProblemIterationInterface::numProblems()
        int numProblems() const override { return 1; }

        /// Return the \ref problem_
        Problem&       problem(int = 0)       { return problem_; }
        Problem const& problem(int = 0) const { return problem_; }

        Problem&       problem(std::string const&)       { return problem_; }
        Problem const& problem(std::string const&) const { return problem_; }

        /// Return a mutable/const view to a solution component
        template <class TreePath = RootTreePath>
        decltype(auto) solution(TreePath path = {}) { return problem_.solution(path); }

        template <class TreePath = RootTreePath>
        decltype(auto) solution(TreePath path = {}) const { return problem_.solution(path); }

        /// compute density on the 2/3 phases
        auto rhoPhase() {
            auto rhoInt = Parameters::get<double>("stokes->density").value_or(0.0);
            auto rhoExt = Parameters::get<double>("stokes->density2").value_or(0.0);
            auto rhoOut = Parameters::get<double>("stokes->densityOutside").value_or(0.0);

            valueOf(*rhoDOF_,_phi) << clamp(getPhase(),0,1)*rhoInt + (1.0 - clamp(getPhase(),0,1))*rhoExt ;
            return valueOf(*rhoDOF_,_phi);
        }

        /// compute viscosity on the 2/3 phases
        auto nuPhase() {
            auto nuInt = Parameters::get<double>("stokes->viscosityPhase").value_or(0.0);
            auto nuExt = Parameters::get<double>("stokes->viscosityOutside").value_or(0.0);
            auto nuInExt = Parameters::get<double>("stokes->viscosityInside").value_or(0.0);

            auto elementVectorIn = ElementVector(*grid(),0);
            auto elementVectorOut = ElementVector(*grid(),0);
            for (auto const& e : elements(gridView())) {
                auto father = e;
                while (father.hasFather())
                    father = father.father();

                if (partitions_[problem().grid()->levelGridView(0).indexSet().index(father)] == 1) {
                    elementVectorIn.data()[gridView().indexSet().index(e)] = 1;
                }
                if (partitions_[problem().grid()->levelGridView(0).indexSet().index(father)] == 0) {
                    elementVectorOut.data()[gridView().indexSet().index(e)] = 1;
                }
            }

            valueOf(*nuDOF_,_phi) << clamp(getPhase(),0.0,1.0)*nuInt
                                     + (1.0 - clamp(getPhase(),0.0,1.0))*nuExt*valueOf(elementVectorOut)
                                       + (1.0 - clamp(getPhase(),0.0,1.0))*nuInExt*valueOf(elementVectorIn);
            return valueOf(*nuDOF_,_phi);
        }

        /// add the ALE Operators
        template<class Container, class LambdaResult>
        void fillCouplingOperatorALE(Container const& laplaceSolution, LambdaResult&& v_cell);

        template <class DGVF, class KV, class EV>
        void fillSurfaceOperators(DGVF const& deltaP,
                                  DGVF const& deltaPhi,
                                  KV const& kappaVecOld,
                                  EV const& normalVec);

        template <class STF, class EV>
        void fillSurfaceTensionForceOperators(STF& stForce,
                                              EV const& normalVec);

        template <class SF, class EV, class DV, class KV, class EVV>
        void fillStretchingForceOperators(SF& stretchingForce,
                                          EV const& distances,
                                          DV const& lambda2,
                                          KV const& kappaVecOld,
                                          EVV const& normalVec);

        template <class BF, class KV, class GF, class EV>
        void fillBendingForceOperators(BF& bendingForce,
                                       KV const& kappaVecOld,
                                       GF const& gaussian,
                                       EV const& normalVec);

        /// set the phase field mobility to mob
        void setMobility(double mob) {
            mobility_ = mob;
        }

        /// set the permeability to P
        void setPermeability(double P) {
            permeability_ = P;
        }


        /// integrate over the shell part of the domain (only for closed shells)
        template <class Expr, class GV>
        auto integrateShell(Expr&& expr,GV const& gridView, std::vector<int> const& partitions, int part=-1);

        /// return the velocity of the shell in x-direction (only for closed shells)
        double const& vx() {
            return vx_;
        }
        /// return the velocity of the shell in y-direction (only for closed shells)
        double const& vy() {
            return vy_;
        }

        ///return the shell volume (only for closed shells)
        double const& volume() {
            return volume_;
        }

        ///set the initial shell area (only for closed shells)
        void setShellArea0(double area) {
            shellArea0_ = area;
        }

        ///set the initial droplet volume area
        void setDropletVolume0(double vol) {
            dropletVolume0_ = vol;
        }

        ///set the initial shell volume area
        void setVolume0(double vol) {
            volume0_ = vol;
        }

        ///get the shell area (only for closed shells)
        double const& getShellArea() {
            return shellArea_;
        }

        ///get the initial droplet volume
        double const& getDropletVolume0() {
            return dropletVolume0_;
        }

        ///get the initial shell volume
        double const& getVolume0() {
            return volume0_;
        }

        ///get the min value for element sizes on the mesh
        double getMinimumElementVolume() {
            return minimumElementVolume;
        }

        std::vector<int> const& partitions() {
            return partitions_;
        }

    protected:
        double rho_ = 1.0, rho2_ = 1.0, rhoOut_ = 1.0;              // densities
        double nu_ = 1.0;                                           // fluid visosity
        double eps_;                                                // epsilon of Cahn-Hilliard equation
        double sigma_;                                              // surface tension of CH
        double mobility_;
        double permeability_;                                       // the shell's permeability
        double volume0_;                                            // initial shell volume (only if closes shell is used)
        double volume_, volChangeOverTime_;                                               // shell area (only if closed shell is used)
        double vx_ = 0.0, vy_ = 0.0;                                           // shell velocity in x-direction
        unsigned int axi;                                           // boolean if axisymmetry is used
        double E_sum;                                               // Gesamtenergie
        double shellArea_, shellArea0_, shellAreaChange_ = 0.0;
        double dropletVolume_,dropletVolume0_, dropVolChangeOverTime_, minimumElementVolume = 1e15;
        std::vector<int> boundaryIDs_, partitions_;                 // information gathered from the mesh: IDs of boundary
                                                                    // segments and elemtent partition IDs
        std::shared_ptr<DOFVector<Basis>> oldSolution_;             // DOFVector for oldSolution
        std::shared_ptr<DOFVector<Basis>> rhoDOF_;                  // DOFVector for density
        std::shared_ptr<DOFVector<Basis>> nuDOF_;                   // DOFVector for viscosity
        std::shared_ptr<DOFVector<VBasis>> oldVelocity_;            // DOFVector for velocity
        std::shared_ptr<DOFVector<PhiBasis>> oldPhi_;               // DOFVector for phase
        std::shared_ptr<DOFVector<PhiBasisCont>> oldPhiContinuous_; // DOFVector for continuous phase
        std::shared_ptr<DOFVector<PhiBasis>> oldMu_;                // DOFVector for mu
        std::shared_ptr<VBasis> vBasis_;
        std::shared_ptr<PhiBasis> phiBasis_;
        std::shared_ptr<PhiBasisCont> phiBasisCont_;
        Problem problem_;                                           // the underlying ProblemStat
        std::string name_;                                          // name of the problem
        double invTau_, tau_;                                       // (inverse of) the time step
        ElementVector<HostGrid,int> partitionsVec, elementLevels;
        ElementVector<HostGrid,double> elementSizes;
    };

} // end namespace AMDiS

#include "NavierStokesCahnHilliard.impl.hpp"
