#pragma once

using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;
using namespace Dune::Indices;

template <typename Writer>
void writePVDFile (std::vector<double> const& timesteps,
                   std::string const& name,
                   std::string const& path,
                   Writer& vtkWriter,
                   int gridDim)
{
    /* remember current time step */
    unsigned int count = timesteps.size();

    std::ofstream pvdFile;
    pvdFile.exceptions(std::ios_base::badbit | std::ios_base::failbit |
                       std::ios_base::eofbit);
    std::string pvdname = path + "/" + name + ".pvd";
    pvdFile.open(pvdname.c_str());
    pvdFile << "<?xml version=\"1.0\"?> \n"
            << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\""
            << Dune::VTK::getEndiannessString() << "\"> \n"
            << "<Collection> \n";
    double timestepSize = 10;
    Parameters::get("adapt->timestep",timestepSize);

    // filename
    std::string fullname;
    for (unsigned int i=0; i<count; i++)
    {
        std::stringstream n;
        n.fill('0');
        n << name << "-" << std::setw(5) << i;

        fullname = n.str();

        pvdFile << "<DataSet timestep=\"" << timesteps[i]
                << "\" group=\"\" part=\"0\" name=\"\" file=\"_piecefiles/"
                << fullname << (gridDim == 1 ? ".vtp" : ".vtu") << "\"/> \n";
    }
    pvdFile << "</Collection> \n"
            << "</VTKFile> \n" << std::flush;
    pvdFile.close();

    //write the file of the actual time step
    vtkWriter.write(path + "/_piecefiles/" + fullname);
}


template <class Writer, class T>
void addWriterData(Writer& writer, T const& arg, std::string name, int dim) {
    if (dim == 1) {
        writer.addVertexData(arg, Dune::VTK::FieldInfo{name, Dune::VTK::FieldInfo::Type::scalar, 1});
    }
    if (dim == 2) {
        writer.addVertexData(arg, Dune::VTK::FieldInfo{name, Dune::VTK::FieldInfo::Type::vector, 2});
    }
}
template <class Writer, class T, class... Types>
void addWriterData(Writer& writer, T const& firstArg, std::string firstName, int firstDim, Types... args) {
    if (firstDim == 1) {
        writer.addVertexData(firstArg, Dune::VTK::FieldInfo{firstName, Dune::VTK::FieldInfo::Type::scalar, 1});
    }
    if (firstDim == 2) {
        writer.addVertexData(firstArg, Dune::VTK::FieldInfo{firstName, Dune::VTK::FieldInfo::Type::vector, 2});
    }
    addWriterData(writer, args...);
}

// writes the data to the surface Writer from the NavierStokesCahnhilliard solution
template <class Writer, class Prob>
void addWriterDataSurface(Writer& writer, Prob&& prob) {
    auto const &spb = prob.solution(_5).basis().preBasis().subPreBasis(_5).subPreBasis();

    auto coordsSurface = surfaceGridFct2D(prob.solution(_4), spb,_4);
    auto laplaceSurface = surfaceGridFct2D(prob.solution(_5), spb,_5);
    auto lambda1 = surfaceGridFct(prob.solution(_6), spb,_6);
    auto lambda2 = surfaceGridFct(prob.solution(_7), spb,_7);
    auto force = surfaceGridFct2D(prob.solution(_8), spb,_8);
    auto vS = surfaceGridFct2D(prob.solution(_9), spb,_9);
    auto phiS = surfaceGridFct(prob.solution(_10), spb,_10);

    addWriterData(writer,coordsSurface,"xS",2,
                  laplaceSurface,"KappaTimesN",2,
                  lambda1,"lambda1",1,
                  lambda2,"lambda2",1,
                  force,"fShell",2,
                  vS,"vS",2,
                  phiS,"phiS",1);
}
