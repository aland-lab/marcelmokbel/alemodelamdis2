#pragma once

using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;
using namespace Dune::Indices;

template <class DV>
void setLambda2EqLambda1Axi(DV const& lambda1, DV& lambda2, std::vector<int> const& partitions) {
    auto const &indexSet = lambda1.basis().gridView().indexSet();
    auto const &indexSet0 = lambda1.basis().gridView().grid().levelGridView(0).indexSet();
    auto localView = lambda2.basis().localView();

    for (auto const &e: elements(lambda1.basis().gridView())) {
        for (const auto &is: intersections(lambda1.basis().gridView(), e)) {
            if (!is.neighbor())
                continue;

            auto father = e;
            while (father.hasFather())
                father = father.father();

            auto fatherOut = is.outside();
            while (fatherOut.hasFather())
                fatherOut = fatherOut.father();

            auto p = partitions[indexSet0.index(father)];
            auto q = partitions[indexSet0.index(fatherOut)];
            if (p != q) {
                localView.bind(e);
                auto const &node = localView.tree();

                for (std::size_t j = 0; j < e.geometry().corners(); ++j) {
                    auto x = e.geometry().corner(j);
                    if (x[1] < 1.e-10) {
                        auto val = lambda1.coefficients().get(localView.index(node.localIndex(j)));
                        lambda2.coefficients().set(localView.index(node.localIndex(j)), val);
                    }
                }
            }
        }
    }
    lambda2.coefficients().finish();
}

template <class GF>
double findVal(GF const& l1S, FieldVector<double,2> const& x) {
    Dune::DiscreteGridViewFunction coords{l1S.basis().gridView(), 1};
    Dune::Functions::interpolate(coords.basis(), coords.coefficients(),
                                 [](auto const &x) { return x; });
    for (int i = 0; i < coords.coefficients().size(); ++i) {
        if ((x - coords.coefficients()[i]).two_norm() < 1.e-7)
            return l1S.coefficients()[i];
    }

    // if point did not exist on old grid, find face, where the point is located in and interpolate values
    auto localView = l1S.basis().localView();

    for (auto const &e: elements(l1S.basis().gridView())) {
        localView.bind(e);
        auto node = localView.tree().child(0);
        auto left = e.geometry().corner(0);
        auto right = e.geometry().corner(1);
        auto face = right - left;
        auto xFace = x - left;
        auto t1 = xFace[0] / face[0];
        auto t2 = xFace[1] / face[1];
        if (std::abs(t1 - t2) < 1.e-7) { // x lies on this face
            auto bulk_idxLeft = localView.index(node.localIndex(0))[0];
            auto bulk_idxRight = localView.index(node.localIndex(1))[0];
            return ((1.0 - t1) * l1S.coefficients()[bulk_idxLeft] + t1 * l1S.coefficients()[bulk_idxRight]);
        }
    }
    // nothing found, which means that a point from the old grid has been removed but a new one has been created by gmsh
    // hence: set value to value of nearest point on old grid
    double dist = 100000;
    int idx = -1;
    for (int i = 0; i < coords.coefficients().size(); ++i) {
        if ((x - coords.coefficients()[i]).two_norm() < dist) {
            dist = (x - coords.coefficients()[i]).two_norm();
            idx = i;
        }
    }
    return l1S.coefficients()[idx];
}

template <class Problem, class CGrid,class SG, class EV, class DV>
bool remeshing(Problem const& nschProb, std::unique_ptr<CGrid>& gridPtr, SG& shellGeometry,
               std::vector<int>& partitions, std::vector<int>& facets,
               std::vector<int>& boundaryIds, std::vector<double> const& timesteps,
               AdaptInfo& adaptInfo, std::string const& path, double const& vMin,
               EV const& distances,
               DV const& lambda2) {
    // perform remeshing
    using Grid = Dune::ALUGrid<2,2,Dune::simplex,Dune::ALUGridRefinementType::conforming>;

    int remeshType = 0;
    int everyRemesh = 1000;
    double minRemeshAngle;
    Parameters::get("remeshing type", remeshType);
    Parameters::get("remeshing every i-th time step", everyRemesh);
    Parameters::get("remeshing at mimimum angle", minRemeshAngle);

    auto const &gridView = nschProb.gridView();
    auto const &gridViewl0 = nschProb.grid()->levelGridView(0);
    if (remeshType == 0 && minAngle(gridView) < minRemeshAngle ||
        (remeshType == 1 && timesteps.size()%everyRemesh == 0)) {

        // define phi as DiscreteGridViewFunction on the surface
        Dune::DiscreteGridViewFunction<decltype(shellGeometry.gridView()), 1> phiDGVF{shellGeometry.gridView(),1};
        phiSurface(phiDGVF, nschProb, shellGeometry, partitions);

        // create new mesh out of surface coordinates
        generateMesh("newMesh", path, shellGeometry ,phiDGVF, adaptInfo.time(), gridViewl0, vMin);

        //store the mesh into the host grid
        MeshCreator<Grid> meshCreatorNew("newMesh");
        auto gridPtrNew = meshCreatorNew.create();
        auto partitionsNew = meshCreatorNew.elementIds();
        auto boundaryIdsNew = meshCreatorNew.boundaryIds();
        auto partitionsOld = partitions;
        partitions = std::move(partitionsNew);
        boundaryIds = std::move(boundaryIdsNew);
        facets.clear();
        facets = computeFacets(*gridPtr,partitions);

        // create a gridFunction with coordinates of the new grid and make CurvedGrid object
        // coordinates DOFVector for remeshing
        GlobalBasis basis2(gridPtrNew->leafGridView(), power<2>(lagrange<1>(),flatInterleaved()));
        auto coordinatesNewGrid = std::make_shared<DOFVector<decltype(basis2)>>(basis2);
        valueOf(*coordinatesNewGrid) << X();
        auto gridNew = Dune::CurvedGrid{*gridPtrNew, valueOf(*coordinatesNewGrid)};

        using Gr = decltype(gridNew);
        auto deg = Parameters::get<int>("polynomial degree ch").value_or(1);

        auto pbfIpol = composite(power<2>(lagrange<2>(),flatInterleaved()),//velocity
                                 lagrange<1>(),//lagrange multiplier for v
                                 surface(lagrange(deg),facets),//phi
                                 surface(lagrange(deg),facets),flatLexicographic());//mu

        int ref_int  = Parameters::get<int>("refinement->interface").value_or(10);
        for (int i = 0; i <= ref_int; ++i) {
            // create InterpolateGrids Object for the saddle point problem
            InterpolateGrids<Gr,decltype(pbfIpol)> ipolProb("ipolProb", gridNew, *nschProb.grid()->hostGrid(), pbfIpol, vMin);
            ipolProb.initialize(INIT_ALL);

            // initialize the problem
            ipolProb.initBaseProblem(adaptInfo);
            ipolProb.initTimeInterface();

            ipolProb.beginIteration(adaptInfo);

            // assemble the matrix
            auto markFlag = ipolProb.problem().markElements(adaptInfo);
            ipolProb.problem().buildAfterAdapt(adaptInfo, markFlag, true, true);
            //ipolProb.oneIteration(adaptInfo);

            auto &&oldVelocity = nschProb.solution(_0);
            auto &&oldPhi = nschProb.solution(_2);
            auto &&oldMu = nschProb.solution(_3);

            //lambda1 on the new grid
            GlobalBasis basis1(ipolProb.problem().gridView(), lagrange<1>());
            auto oldLamb = makeDOFVector(basis1);

            GlobalBasis basisSubdomains(ipolProb.problem().gridView(), power<1>(surface(lagrange(deg),facets),flatInterleaved()));

            auto inside = Parameters::get<int>("phase field inside").value_or(0);
            auto outside = Parameters::get<int>("phase field outside").value_or(0);
            // assemble the RHS of the system for velocity on the new grid
            assembleRHSVec(ipolProb.glue_,*ipolProb.problem().rhsVector(), oldVelocity, _0);
            if (outside) {
                assembleRHSAvg(ipolProb.glue_,*ipolProb.problem().rhsVector(), oldPhi, _2, partitions, partitionsOld);
                assembleRHSAvg(ipolProb.glue_,*ipolProb.problem().rhsVector(), oldMu, _3, partitions, partitionsOld);
            }
            if (inside) {
                assembleRHSAvg(ipolProb.glue_,*ipolProb.problem().rhsVector(), oldPhi, _2, partitions, partitionsOld,1);
                assembleRHSAvg(ipolProb.glue_,*ipolProb.problem().rhsVector(), oldMu, _3, partitions, partitionsOld,1);
            }

            // solve the problem
            ipolProb.problem().solve(adaptInfo, true, false);
            ipolProb.problem().estimate(adaptInfo);
            ipolProb.closeTimestep(adaptInfo);
            ipolProb.endIteration(adaptInfo);

            correctPhaseShell(ipolProb.solution(_2),phiDGVF,_2,partitions,partitionsOld);
            if (i <= ref_int) {
                ipolProb.problem().markElements(adaptInfo);
                ipolProb.problem().adaptGrid(adaptInfo);
            }

            ipolProb.writeFiles(adaptInfo);

            // store oldVelocity into grid function
            GlobalBasis basis2D(ipolProb.problem().gridView(), power<2>(lagrange<2>(),flatInterleaved()));
            auto oldV = makeDOFVector(basis2D);
            auto oldPh = makeDOFVector(basisSubdomains);
            auto oldM = makeDOFVector(basisSubdomains);

            valueOf(oldV) << ipolProb.solution(_0);
            valueOf(oldPh) << ipolProb.solution(_2);
            valueOf(oldM) << ipolProb.solution(_3);

            oldV.backup(path + "/backup/uhOld" + std::to_string(adaptInfo.time()));
            oldPh.backup(path + "/backup/phiOld" + std::to_string(adaptInfo.time()));
            oldM.backup(path + "/backup/muOld" + std::to_string(adaptInfo.time()));
        }

        // interpolate lambda1 + lambda2 to new grid
        {
            auto incompressibleShell = Parameters::get<int>("is shell incompressible").value_or(0);
            auto axi = Parameters::get<int>("axisymmetric").value_or(0);
            GlobalBasis basisNew(gridPtrNew->leafGridView(), lagrange<1>());
            auto lambda1New = makeDOFVector(basisNew);
            auto lambda2New = makeDOFVector(basisNew);

            if (!incompressibleShell) {
                DOFVector lambda1S(nschProb.gridView(),
                                   power<1>(Dune::Surfacebasis::surfaceLagrange<1>(partitionsOld), flatInterleaved()));
                DOFVector lambda1SNew(gridPtrNew->leafGridView(),
                                      power<1>(Dune::Surfacebasis::surfaceLagrange<1>(partitions), flatInterleaved()));
                DOFVector lambda2S(nschProb.gridView(),
                                   power<1>(Dune::Surfacebasis::surfaceLagrange<1>(partitionsOld), flatInterleaved()));
                DOFVector lambda2SNew(gridPtrNew->leafGridView(),
                                      power<1>(Dune::Surfacebasis::surfaceLagrange<1>(partitions), flatInterleaved()));

                // interpolate lambdas to surfaceLagrange DOFVector
                valueOf(lambda1S).interpolate(valueOf(distances), tag::average{});
                valueOf(lambda2S) << valueOf(lambda2);

                // interpolate then to the surface grid to do the interpolation to new grid there
                auto const &spb = lambda1S.basis().preBasis().subPreBasis();
                auto l1S = surfaceGridFct(lambda1S, spb, _0);
                auto l2S = surfaceGridFct(lambda2S, spb, _0);

                auto const &spbNew = lambda1SNew.basis().preBasis().subPreBasis();
                auto l1SNew = surfaceGridFct(lambda1SNew, spbNew, _0);
                auto l2SNew = surfaceGridFct(lambda2SNew, spbNew, _0);

                Dune::DiscreteGridViewFunction coords{l1SNew.basis().gridView(), 1};
                Dune::Functions::interpolate(coords.basis(), coords.coefficients(),
                                             [](auto const &x) { return x; });

                // the actual interpolation step
                for (int i = 0; i < coords.coefficients().size(); ++i) {
                    auto x = coords.coefficients()[i];

                    auto val = findVal(l1S, x);
                    l1SNew.coefficients()[i] = val;

                    if (axi) {
                        auto val2 = findVal(l2S, x);
                        l2SNew.coefficients()[i] = val2;
                    }
                }

                // now bridn the data back top the bulk, first to a DiscreteGridViewFunction, then to the DOFVector
                using GV = decltype(gridPtrNew->leafGridView());
                Dune::DiscreteGridViewFunction<GV,1> aux1{lambda1New.basis().gridView(), 1};
                Dune::DiscreteGridViewFunction<GV,1> aux2{lambda1New.basis().gridView(), 1};
                for (auto it: spbNew.surfaceToFluidMapIdx()) {
                    aux1.coefficients()[it.second] = l1SNew.coefficients()[it.first];
                    aux2.coefficients()[it.second] = l2SNew.coefficients()[it.first];
                }
                valueOf(lambda1SNew) << aux1 - 1.0;
                valueOf(lambda2SNew) << aux2 - 1.0;

                // then make the DOFVectors readable for outputs, by setting them to 1.0
                // everywhere and interpolate surface values afterward
                valueOf(lambda1New).interpolate(valueOf(lambda1SNew, _0), tag::assignToBulk{});
                valueOf(lambda1New) += 1.0;
                valueOf(lambda2New).interpolate(valueOf(lambda2SNew, _0), tag::assignToBulk{});
                valueOf(lambda2New) += 1.0;

                // correct lambda2 on the symmetry axis
                if (axi) {
                    setLambda2EqLambda1Axi(lambda1New, lambda2New, partitions);
                }
            } else {
                valueOf(lambda1New) << 1.0;
                valueOf(lambda2New) << 1.0;
            }
            lambda1New.backup(path + "/backup/lambda1Old" + std::to_string(adaptInfo.time()));
            lambda2New.backup(path + "/backup/lambda2Old" + std::to_string(adaptInfo.time()));
        }

        //update grid to new one
        gridPtr.swap(gridPtrNew);
        std::string filename(path + "/backup/gridRefined" + std::to_string(adaptInfo.time()));
        std::ofstream file( filename.c_str());
        gridPtr->hostGrid()->backup(file,ALUGrid::MacroFileHeader::Format::ascii);

        // leave the current loop and start over with the new one on the new grid
        return true;
    }
    return false;
}