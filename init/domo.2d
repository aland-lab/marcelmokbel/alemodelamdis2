L: 0.00001
T: 0.000001

% ========== shell parameter ====================================

%explicit or implicit strategy?
explicit surface tension force: 0        % 0 if implicit, 1 if explicit
explicit bending force: 0                % 0 if implicit, 1 if explicit
explicit stretching force: 0             % 0 if implicit, 1 if explicit
closed shell: 1                          %1 if shell is closed, 0 else (only for simulation speed, optimization for closed shells)

%shell force parameters
surfaceTension: 0                          %3 % 1 % = 0.0010 * $T*$T/$L/$L/$L
bendingStiffness: 8e-20*$T*$T/($L*$L*$L*$L*$L)                   %0.00111         %6.67e9 %11.1 * 1e-15 * $T*$T/$L/$L/$L/$L/$L
areaShear: 0.0                             %1.e7 % 0.008333 * $T*$T/$L/$L/$L
areaDilation: 0                          %25.0 % 3.e7 % 0.025 * $T/*$T/$L$L/$L
zeroReferenceCurvature: 0                  % 1 if reference curvature is zero, 0 else (EXPLICIT BENDING ONLY!)
test for elastic only: 1.0

correct point distances: 0                 %1 if points on interface and shell should be kept in equal distances

% ========== inextensibility =====================================
is shell incompressible: 0                 %1 if incompressible shell, 0 else
surface inextensibility diffusion: 0.0     %1.e-9
surface concentration relaxation: 0.0

% ========== surface tensions (DOMO) ====================================
parameters->sigma:  15*1e-3*$T*$T/($L*$L*$L)          % 30mN/m -> umrechnungsfaktor 10^6*2*pi
parameters->sigma1: 11.25*1e-3*$T*$T/($L*$L*$L)             % die werte so einstellen, dass
parameters->sigma0: -0.75*1e-3*$T*$T/($L*$L*$L)        % der gewünschte Kontaktwinkel eingestellt wird

% ========== permeability ===============================================
parameters->permeability: 0*2.5e-3          %m/s => 10^-8 m²s/kg -> 1e-8*$L/$T
phi jump threshold: 0.04                %only phi values further away from the interface are considered for jump of phi over \Gamma


% ========== fluid parameter ===================================
stokes->viscosity:  0.001*$T/$L/$L                 % =1.0 * $T/$L/$L, internal viscosity (phi = 1)
stokes->viscosity2: 0.001*$T/$L/$L                 % =1.0 * $T/$L/$L, external viscosity (phi = 0)
stokes->density: 1000.0                  % inside phase field (phi = 1)
stokes->density2: 1000.0                 % outside phase field (phi = 0)
stokes->densityOutside: 1000.0           % outside shell
gravity: 0                               % 1962          % *$T*$T/$L % gravitational accelaration
direction: 1 -0.15

parameters->keep membrane centered: 0    %1 if membrane should stay in center of domain, 0 else

%=========== remeshing parameter =====================================

remeshing type: 0                        % 0 for remeshing at minimum angle, 1 for time step controlled remeshing
remeshing every i-th time step: 300000   % perform a remeshing step in every i-th time step
remeshing at mimimum angle: 0.08          % 0.392699 % pi/8
remeshing at maximum element stretch: 10000
CFL: 0.17                                % maximum ratio of velocity element element length before remeshing

line mesh: 1
mesh resolution at shell: 0.01
mesh resolution at boundary: 0.015
mesh scaling factor: 1                   % scale the mesh with this factor
mesh xScale: 0.5*${mesh scaling factor}    % size of the internal box in x direction (for non line mesh only)
mesh yScale: 0.5*${mesh scaling factor}    % size of the internal box in y direction (for non line mesh only)

%if points are closer to each other than this distance, they get removed
remove interface points: 0
distance for removal of interface points:  0.0 %0.0002

reduce time step after remeshing: 1      % after remeshing... set 1 if time step size should be reduced, 0 else
adapt->time step reduction factor: 0.01   % set the factor by which the time step size should be reduced, 1: no reduction
number of time steps for reduction: 5    % set the numer of time steps for which the time step size should be reduced

%=========== initial phase field parameters ==============================

parameters->number of circles: 1

% first circle (DOMO)
parameters->radius1_0: 0.007071
parameters->radius2_0: 0.007071
parameters->center0:  0.0 0.03 % 0.07
cell height: -32.5                        % no phase field below this value

%second circle
parameters->radius1_1: 0.065
parameters->radius2_1: 0.065
parameters->center1: 0.25 0.0 % 0.07

%third circle
parameters->radius1_2: 0.075
parameters->radius2_2: 0.075
parameters->center2: -0.23 -0.1 % 0.07

%fourth circle
parameters->radius1_3: 0.045
parameters->radius2_3: 0.045
parameters->center3: -0.18 0.18 % 0.07

%fifth circle
parameters->radius1_4: 0.13
parameters->radius2_4: 0.13
parameters->center4: 0.0 -0.25 % 0.07

% =========== phase field parameters =============================

droplet volume conservation parameter: 0
polynomial degree ch: 2

phase field inside: 0                      % 1 if phase field should be present inside
phase field outside: 1                     % 1 if phase field should be present outside
circles or initial value: 0                % 0 if circles, 1 if initial value
phase field initial value: 0.2             % initial value of no circles
phase field initial value random: 0.1      % initial value of no circles
initial radius: 10.2                       % radius within initial phase field should be located

use capillary stress: 1                    % 1 if capillary stress should be used, 0 if modified coupling term
parameters->couplingSign3: 0               % negative is correct if capillary stress is 0 (or zero, if capillary stress is 1)

parameters->eps: 0.0001                    %(DOMO)

parameters->M0: 2.e-10 % in [m³s/kg]       % initial mobility
parameters->M: 2.e-10 % in [m³s/kg]        % mobility after remeshing (for a few time steps)


% DOMO
refinement->interface: 11                  % wenn du das um 1 erhöhst, musst du die element size halbieren
refinement->bulk: 0
refinement->refine with stretch: 0         % = 1 if refinement after strong stretch on the surface is desired

% ========== other ==============================================

level of information: 1

volume restore constant: 0.125
axisymmetric: 0
sign0:0
sign1:0
axisymmetric dirichlet: 1
axisymmetric dirichlet kappaVecOld: 1

% ========== Output Info ==============================================

output directory: ../ResultsALEModelAmdis2/testSusanneNoStretch2
output_filename: energy_output

parameter file name: ./init/domo.2d
parameter output file name: ${output directory}/parameters.2d

every: 1
every2: 10000

stokesMesh->macro file name: ./macro/line.msh
stokes->mesh: newMesh
newMesh->macro file name: ${output directory}/newMesh.msh
newMeshRestore->macro file name: ${output directory}/backup/newMesh0.024000.msh
stokesMesh->global refinements: 0

stokes->space->solver: direct
stokes->space->solver->relative tolerance: 1e-8
stokes->space->solver->info:	-1

laplace->space->solver: direct
laplace->space->solver->relative tolerance: 1e-8
laplace->space->solver->info:	-1

ipolProb->space->solver: direct
ipolProb->space->solver->relative tolerance: 1e-8
ipolProb->space->solver->info:	-1

kappaVecOld->solver: direct
kappaVecOld->solver->relative tolerance: 1e-8
kappaVecOld->solver->info:	-1

stokes->space->output[0]->format: vtk
stokes->space->output[0]->filename: stokes_u.2d
stokes->space->output[0]->name: u
stokes->space->output[0]->subsampling: 2
stokes->space->output[0]->output directory: ${output directory}
stokes->space->output[0]->mode: 1
stokes->space->output[0]->animation: 1
stokes->space->output[0]->write every i-th timestep:	$every2

stokes->space->output[1]->format: vtk
stokes->space->output[1]->filename: stokes_p.2d
stokes->space->output[1]->name: p
stokes->space->output[1]->output directory: ${output directory}
stokes->space->output[1]->mode: 1
stokes->space->output[1]->animation: 1
stokes->space->output[1]->write every i-th timestep:	$every2

laplace->space->output[0]->format: vtk
laplace->space->output[0]->filename: laplaceX.2d
laplace->space->output[0]->name: laplace
laplace->space->output[0]->output directory: ${output directory}
laplace->space->output[0]->mode: 1
laplace->space->output[0]->animation: 1
stokes->space->output[0]->write every i-th timestep:	$every2

laplace->space->output[1]->format: vtk
laplace->space->output[1]->filename: laplaceY.2d
laplace->space->output[1]->name: laplace
laplace->space->output[1]->output directory: ${output directory}
laplace->space->output[1]->mode: 1
laplace->space->output[1]->animation: 1
stokes->space->output[0]->write every i-th timestep:	$every2

ipolProb->space->output[0]->format: vtk
ipolProb->space->output[0]->filename: ipolProb.2d
ipolProb->space->output[0]->name: ipolProb
ipolProb->space->output[0]->output directory: ${output directory}
ipolProb->space->output[0]->mode: 1
ipolProb->space->output[0]->animation: 1
ipolProb->space->output[0]->write every i-th timestep:	$every

ipolProb->space->output[2]->format: vtk
ipolProb->space->output[2]->filename: ipolProb_phi.2d
ipolProb->space->output[2]->name: phi
ipolProb->space->output[2]->output directory: ${output directory}
ipolProb->space->output[2]->mode: 1
ipolProb->space->output[2]->animation: 1
ipolProb->space->output[2]->write every i-th timestep:	$every

ipolProb->space->output[3]->format: vtk
ipolProb->space->output[3]->filename: ipolProb_lambda1.2d
ipolProb->space->output[3]->name: lambda1
ipolProb->space->output[3]->output directory: ${output directory}
ipolProb->space->output[3]->mode: 1
ipolProb->space->output[3]->animation: 1
ipolProb->space->output[3]->write every i-th timestep:	$every


adapt->max iteration: 10   % maximal number of fixed point iterations
adapt[0]->tolerance: 1.e-3
adapt->max timestep iteration: 1
adapt->max time iteration: 1
adapt->strategy: 0

% DOMO
adapt->timestep: 0.0005 % 2.5e-2 %0.5e-6
adapt->start time: 0.0 %187.6
adapt->end time: 500000
